<?php
    class Ville{
        // Variables
            private $_ID;
            private $_Maire;
            private $_Name;
            private $_Money;
            private $_Population;
            private $_Industrie;
            private $_Transport;
            private $_Security;
            private $_Environement;
            private $_Commerce;

            private $_PourCentChomage;
            private $_PourCentTransport;
            private $_PourCentSecurity;
            private $_PourCentEnvironement;


        // Functions Construct
            public function __construct($bdd,$Maire){
                $this->_bdd = $bdd;
                $this->_Maire = $Maire;

                $ReqBDD = $this->_bdd->prepare("SELECT * FROM `ville` WHERE `Maire` = ?");
                $ReqBDD->execute(array($Maire));
                while($Ville_Tab = $ReqBDD->fetch(PDO::FETCH_ASSOC)){
                    $this->_Name = $Ville_Tab['Name'];
                    $this->_Money = $Ville_Tab['Money'];
                    $this->_Population = $Ville_Tab['Population'];
                    $this->_Industrie = $Ville_Tab['Industrie'];
                    $this->_Transport = $Ville_Tab['Transport'];
                    $this->_Security = $Ville_Tab['Security'];
                    $this->_Environement = $Ville_Tab['Environement'];
                    $this->_Commerce = $Ville_Tab['Commerce'];
                }

                $this->_PourCentChomage         =   0;
                $this->_PourCentTransport       =   0;
                $this->_PourCentSecurity        =   0;
                $this->_PourCentEnvironement    =   0;
            }


        // Functions Returns Top / Classement
            public function ReturnTopPopulation($_ID){
                return $this->_Population;
            }
            public function ReturnTopMoney($_ID){
                return $this->_Money;
            }


        // Functions Returns Values
            public function ReturnID(){
                return $this->_ID;
            }
            public function ReturnMaire(){
                return $this->_Maire;
            }
            public function ReturnName(){
                return $this->_Name;
            }
            public function ReturnMoney(){
                return $this->_Money;
            }
            public function ReturnPopulation(){
                return $this->_Population;
            }
            public function ReturnIndustrie(){
                return $this->_Industrie;
            }
            public function ReturnTransport(){
                return $this->_Transport;
            }
            public function ReturnSecurity(){
                return $this->_Security;
            }
            public function ReturnEnvironement(){
                return $this->_Environement;
            }
            public function ReturnCommerce(){
                return $this->_Commerce;
            }


        // Functions Returns Pourcentage
            public function ReturnPourCentChomage(){
                return $this->_PourCentChomage;
            }
            public function ReturnPourCentTransport(){
                return $this->_PourCentTransport;
            }
            public function ReturnPourCentSecurity(){
                return $this->_PourCentSecurity;
            }
            public function ReturnPourCentEnvironement(){
                return $this->_PourCentEnvironement;
            }


        // Functions Ville Fleurie
            public function StatutVilleFleurie(){
                if($this->_PourCentEnvironement >= 100 && $this->_Population >= 10000){
                    ?>
                        <div class='CityFlower'>
                            <img src="img/City_Flower/4.png" alt="Ville 4 Fleurs">
                        <div>
                    <?php
                }
                else if($this->_PourCentEnvironement >= 80 && $this->_Population >= 5000){
                    ?>
                        <div class='CityFlower'>
                            <img src="img/City_Flower/4.png" alt="Ville 4 Fleurs">
                        <div>
                    <?php
                }
                else if($this->_PourCentEnvironement >= 60 && $this->_Population >= 2000){
                    ?>
                        <div class='CityFlower'>
                            <img src="img/City_Flower/3.png" alt="Ville 3 Fleurs">
                        <div>
                    <?php
                }
                else if($this->_PourCentEnvironement >= 40 && $this->_Population >= 500){
                    ?>
                        <div class='CityFlower'>
                            <img src="img/City_Flower/2.png" alt="Ville 2 Fleurs">
                        <div>
                    <?php
                }
                else if($this->_PourCentEnvironement >= 20 && $this->_Population >= 100){
                    ?>
                        <div class='CityFlower'>
                            <img src="img/City_Flower/1.png" alt="Ville 1 Fleur">
                        <div>
                    <?php
                }
                else{
                    ?>
                        <div class='CityFlower'>
                            <img src="img/City_Flower/0.png" alt="Ville Sans Fleurs">
                        <div>
                    <?php
                }
            }


        //  Functions Mise à jours




        // Valeur équivalence
        //
        //      Money : 
        //
        //  1 Population    =   150€    // Vérifié : 100€ à 150€
        //  1 Industrie     =   250€    // Vérifié : 250€
        //  1 Transport     =   0€      // Vérifié : 0€
        //  1 Security      =   0€      // Vérifié : 0€
        //  1 Environement  =   0€      // Vérifié : 0€
        //  1 Commerce      =   400€    // Vérifié : 400€
        //
        //
        //      Chomage :
        //
        //  1 Population    =   +1,5    points
        //  1 Commerce      =   -1,5    points
        //  1 Industrie     =   -3      points
        //  1 Transport     =   -0,25   points
        //  1 Security      =   -0,25   points
        //
        //
        //      Transport :
        //
        //  1 Transport     =   +2
        //  1 Population    =   -0,5
        //  1 Commerce      =   -0,5
        //  1 Industrie     =   -1
        //
        //
        //      Security :
        //
        //  1 Security      =   +3
        //  1 Population    =   -0,75
        //  1 Commerce      =   -0,5
        //  1 Industrie     =   -1,5
        //  1 Transport     =   -0,25
        //
        //
        //      Environement :
        //
        //  1 Environement  =   +4
        //  1 Population    =   -0,25
        //  1 Commerce      =   -0,5
        //  1 Industrie     =   -2,5
        //  1 Transport     =   -0,75
        //
    }
?>