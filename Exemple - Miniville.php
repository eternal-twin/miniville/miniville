<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="fr" xml:lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
		<title>EXEMPLE - Miniville</title>

		<link href="css/style.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript" src="js/sfw.js"></script>
		<script type="text/javascript" src="js/js.js"></script>
		<link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&subset=latin,cyrillic" type="text/css" rel="stylesheet"/>
		<link href="/rss" type="application/rss+xml" rel="alternate" title="Rss feed"/>
		<link href="/microsummary" type="application/x.microsummary+xml" rel="microsummary"/>
	</head>
	<body>
		<div id="global">
			<div onclick="document.location='http://hajimeville.miniville.fr/'" id="header">			
				<div class="swf" id="swf_box">
				</div>
				<script type="text/javascript">var sobox = new SWFObject("http://pub.motion-twin.com/popotamo/fr/230x200.swf","box",132,115,8,"#000000");
					sobox.addParam("menu","false");
					sobox.addParam("AllowScriptAccess","always");
					sobox.addVariable("url","http://trax.motion-twin.com/goto/mini_box/popo");
					haxe.Timer.delay(function(){ sobox.write("swf_box") },15000);
				</script>
				<div id="city_name">	
					<div class="swf" id="swf_name">
						<p>Votre lecteur Flash n'est pas à jour. <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=French&P5_Language=French" target="_blank">Installer la dernière version</a></p>
					</div>
					<script type="text/javascript">
						var so = new SWFObject("http://data.miniville.fr/swf/name.swf?v=1","name",210,120,8,"#deecfe");
						so.addParam("menu","false");
						so.addParam("wmode","transparent");
						so.addParam("AllowScriptAccess","always");
						so.addParam("FlashVars","text=HajimeVille&pop=4849446&pol=0&crim=0&env=1060761");
						so.write("swf_name");
					</script>
				</div>
			</div>
			<div id="city">
				<div id="lineup">
					<div id="content">
						<div class="menu">
							<div class="swf" id="swf_rank">
								Votre lecteur Flash n'est pas à jour. <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=French&P5_Language=French" target="_blank">Installer la dernière version</a>
							</div>
							<script type="text/javascript" id="exec-js">
								var so = new SWFObject("http://data.miniville.fr/swf/rank.swf?v=0","rank",130,260,8,"#deecfe");
								so.addParam("menu","false");
								so.addParam("AllowScriptAccess","always");
								so.addParam("FlashVars","defaultSelection=1&ranking=1,ninitoons76|2,angejackville|3,hajimeville|4,sausseuzemare|5,wil76ville|6,le-grandq|7,herbachat");
								so.addParam("scale","noscale");
								so.write("swf_rank");
							</script>
							<a href="/ranking?region=1" class="button largeBtn">Classement</a>
							<a href="/search" class="button largeBtn">Voir une ville</a>
							<a href="http://miniville.fr/" class="button largeBtnRed">Créer ma ville</a>
						</div>
						<div class="swf" id="swf_client">
							Votre lecteur Flash n'est pas à jour. <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=French&P5_Language=French" target="_blank">Installer la dernière version</a>
						</div>
						<script type="text/javascript" id="exec-js">
							var so = new SWFObject("http://data.miniville.fr/swf/client.swf?v=5","client",600,400,8,"#deecfe");
							so.addParam("menu","false");
							so.addParam("AllowScriptAccess","always");
							so.addParam("FlashVars","name=HajimeVille&pop=4849446&ind=1594714&tra=977437&sec=1221304&env=1060761&com=99386&k=eff8f");
							so.addParam("scale","noscale");
							so.write("swf_client");
						</script>
						<div id="bottom">
							<div class="links">
								<p>Pour améliorer EXEMPLE, il suffit de donner les liens suivants à vos amis, ou les poster sur votre blog. Pour chaque clic différent, HajimeVille prospérera !</p>
								<div class="link" onmouseover="mt.js.Tip.show(this,'Chaque visite sur la page &lt;strong&gt;http://hajimeville.miniville.fr/&lt;/strong&gt; augmente la population de HajimeVille.&lt;p class=&quot;info&quot;&gt;Une seule action par personne et par jour&lt;/p&gt;','largeTip')" onmouseout="mt.js.Tip.hide()">
									<div class="nb">1</div>
									<div>
										<strong>Augmenter la population</strong>
										<p>http://EXEMPLE.miniville.fr</p>
									</div>
								</div>
								<div class="link" onmouseover="mt.js.Tip.show(this,'Chaque visite sur la page &lt;strong&gt;http://hajimeville.miniville.fr/ind/&lt;/strong&gt; augmente l\'industrie de HajimeVille.&lt;p class=&quot;info&quot;&gt;Une seule action par personne et par jour&lt;/p&gt;','largeTip')" onmouseout="mt.js.Tip.hide()">
									<div class="nb">2</div>
									<div>
										<strong>Augmenter l'industrie</strong>
										<p>http://EXEMPLE.miniville.fr/ind</p>
									</div>
								</div>
								<div class="link" onmouseover="mt.js.Tip.show(this,'Chaque visite sur la page &lt;strong&gt;http://hajimeville.miniville.fr/tra/&lt;/strong&gt; améliore le réseau de transports de HajimeVille.&lt;p class=&quot;info&quot;&gt;Une seule action par personne et par jour&lt;/p&gt;','largeTip')" onmouseout="mt.js.Tip.hide()">
									<div class="nb">3</div>
									<div>
										<strong>Améliorer le réseau de transports</strong>
										<p>http://EXEMPLE.miniville.fr/tra</p>
									</div>
								</div>
								<div class="link" onmouseover="mt.js.Tip.show(this,'Chaque visite sur la page &lt;strong&gt;http://hajimeville.miniville.fr/sec/&lt;/strong&gt; améliore la sécurité à HajimeVille.&lt;p class=&quot;info&quot;&gt;Une seule action par personne et par jour&lt;/p&gt;','largeTip')" onmouseout="mt.js.Tip.hide()">
									<div class="nb">4</div>
									<div>
										<strong>Augmenter la sécurité</strong>
										<p>http://EXEMPLE.miniville.fr/sec</p>
									</div>
								</div>
								<div class="link" onmouseover="mt.js.Tip.show(this,'Chaque visite sur la page &lt;strong&gt;http://hajimeville.miniville.fr/env/&lt;/strong&gt; améliore l\'environnement HajimeVille.&lt;p class=&quot;info&quot;&gt;Une seule action par personne et par jour&lt;/p&gt;','largeTip')" onmouseout="mt.js.Tip.hide()">
									<div class="nb">5</div>
									<div>
										<strong>Améliorer l'environnement</strong>
										<p>http://EXEMPLE.miniville.fr/env</p>
									</div>
								</div>
								<div class="link" onmouseover="mt.js.Tip.show(this,'Chaque visite sur la page &lt;strong&gt;http://hajimeville.miniville.fr/com/&lt;/strong&gt; augmente les commerces à HajimeVille.&lt;p class=&quot;info&quot;&gt;Une seule action par personne et par jour&lt;/p&gt;','largeTip')" onmouseout="mt.js.Tip.hide()">
									<div class="nb">6</div>
									<div>
										<strong>Augmenter les commerces</strong>
										<p>http://EXEMPLE.miniville.fr/com</p>
									</div>
								</div>
							</div>
							<div class="histo">
								<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/birth.gif"/>
									<div>
										<p><strong>03 juin : </strong>Un nouvel habitant a rejoint EXEMPLE.</p>
									</div>
								</div>
								<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/newind.gif"/>
									<div>
										<p><strong>03 juin : </strong>Une nouvelle usine vient d'ouvrir.</p>
									</div>
								</div>
								<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/birth.gif"/>
									<div>
										<p><strong>03 juin : </strong>Un nouvel habitant a rejoint EXEMPLE.</p>
									</div>
								</div>
								<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/env.gif"/>
									<div>
										<p><strong>03 juin : </strong>De nouveaux espaces verts ont été aménagés dans la EXEMPLE.</p>
									</div>
								</div>
									<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/birth.gif"/>
									<div>
										<p><strong>03 juin : </strong>Un nouvel habitant a rejoint EXEMPLE.</p>
									</div>
								</div>
								<div class="evt ">
								<img src="http://data.miniville.fr/img/icons/env.gif"/>
									<div>
										<p><strong>03 juin : </strong>De nouveaux espaces verts ont été aménagés dans la ville.</p>
									</div>
								</div>
								<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/birth.gif"/>
									<div>
										<p><strong>03 juin : </strong>Un nouvel habitant a rejoint EXEMPLE.</p>
									</div>
								</div>
								<div class="evt ">
									<img src="http://data.miniville.fr/img/icons/birth.gif"/>
									<div>
										<p><strong>03 juin : </strong>Un nouvel habitant a rejoint EXEMPLE.</p>
									</div>
								</div>
							</div>
							<div class="stats">
								<dl class="one first">
									<dt>Population</dt>
									<dd>4849446</dd>
								</dl>
								<dl class="one">
									<dt>Revenus</dt>
									<dd>1165849800 €</dd>
								</dl>
								<div class="clear"></div>
								<dl class="list">
									<div class="space5"></div>
									<dt onmouseover="mt.js.Tip.show(this,'Vos habitants ont besoin de travail ! Augmentez l\'industrie pour conserver un faible taux de chomage.',null)" onmouseout="mt.js.Tip.hide()">Chomage</dt>
									<dd class="ok">0 %</dd>
									<dt onmouseover="mt.js.Tip.show(this,'Améliorez le réseau de transport pour que vos habitants puissent se déplacer facilement.',null)" onmouseout="mt.js.Tip.hide()">Transport</dt>
									<dd class="ok">100 %</dd>
									<dt onmouseover="mt.js.Tip.show(this,'Vos habitants veulent se sentir en sécurité ! Augmentez la sécurité pour conserver un faible taux de criminalité.',null)" onmouseout="mt.js.Tip.hide()">Criminalité</dt>
									<dd class="ok">0 %</dd>
									<dt onmouseover="mt.js.Tip.show(this,'Vive l\'air pur ! Améliorez l\'environnement pour conserver un taux de pollution au plus bas.',null)" onmouseout="mt.js.Tip.hide()">Pollution</dt>
									<dd class="ok">0 %</dd>
								</dl>
								<div class="img_box" onmouseover="mt.js.Tip.show(this,'&lt;strong&gt;Popotamo&lt;/strong&gt; Le seul jeu de mots multijoueurs qui s\'adapte à votre emploi du temps !','neutralTip')" onmouseout="mt.js.Tip.hide()">
									<a href="http://trax.motion-twin.com/goto/mini_imgbas/popo" target="_blank"><img border="0" alt="Popotamo" src="http://data.miniville.fr/img/ads/popotamo.gif"/></a>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<div id="links">
				<p>Eternal-Twin est un site web non-commercial géré par des amateurs. Son but est d'archiver les jeux de Motion Twin, c'est à dire les garder accessibles même en cas de fermeture du site officiel. Nous ne sommes pas liés à Motion-Twin.</p>
				<!--
					<a href="http://trax.motion-twin.com/goto/mini_link/drpg" target="_blank" title="Adoptez des Dinoz pour parcourir les terres de Dinoland ! Elevez-les, entraînez-les et devenez le plus puissant des Maîtres de Dinoz !">DinoRPG</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/alpha" target="_blank" title="Des planètes inexplorées, des rencontres inattendues, un gros paquet de briques à exploser, n'hésitez plus : Tentez l'expérience du premier casse-brique-rpg de l'histoire du jeu-vidéo en ligne !">AlphaBounce</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/hfest" target="_blank" title="Un véritable jeu d'arcade captivant, élu meilleur jeu flash 2006 !">Hammerfest</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/dcard" target="_blank" title="Collectionnez vos cartes et combattez les autres joueurs !">Dinocard</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/kk" target="_blank" title="Jouez à plus de 40 jeux de qualité et gagnez des cadeaux !">KadoKado</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/dparc" target="_blank" title="Guidez vos dinoz, entraînez-les et menez-les à la victoire au fil des combats contre ceux des autres joueurs !">Dinoparc</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/popo" target="_blank" title="Le seul jeu de mots multijoueurs qui s'adapte à votre emploi du temps !">Popotamo</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/croque" target="_blank" title="Un site de gestion multi-joueurs effrayant, fun et déconseillé aux moins de 12 ans !">Croquemonster</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/hordes" target="_blank" title="Un jeu de survie communautaire dans un monde hostile peuplé de créatures mort-vivantes.">Hordes</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/muxxu" target="_blank" title="Des jeux web variés pour tous les goûts, des communautés actives et passionantes.">Muxxu</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/intrusion" target="_blank" title="Oubliez toutes les règles, c'est vous qui avez le contrôle ! Plongez dans la scène underground la plus fermée du réseau.">Intrusion</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/kingdom" target="_blank" title="Faites prospérer votre capitale, dominez vos vassaux, agrandissez votre royaume et devenez... Empereur !">Kingdom</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/fever" target="_blank" title="Des fruits, de la fièvre, tout s'enchaîne ! Une centaine de mini-jeux absurdes qui ne vous laisseront pas de répit !">Fever!</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/kube" target="_blank" title="Explorez un monde sauvage, collectez des kubes pour modifier votre environnement et devenez un grand architecte !">Kube</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/majo" target="_blank" title="Découvrez un quiz hors norme : il n'y a pas de bonne ou de mauvaise réponse, seul compte l'avis de la majorité !">Majority</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/arka" target="_blank" title="Votre salle d'arcade en ligne : jouez à plein de jeux déjantés !">Arkadeo</a>
					<a href="http://trax.motion-twin.com/goto/mini_link/mush_fr" target="_blank" title="Perdu dans l'espace, poursuivi par l'infâme Mush, comment allez-vous fuir ?">Mush</a>
				-->
			</div>
			&copy; 2007 <a href="http://www.motion-twin.com" target="_blank"><img alt="Motion-Twin" src="http://data.miniville.fr/img/motiontwin.gif"/></a>
			<a href="http://haxe.org/" target="_blank"><img alt="haXe powered" src="http://data.miniville.fr/img/haxe.png" title="Ce site a été développé en haXe !"/></a>
		</div>
	</body>
</html>

